use std::process::exit;
use std::fs::{File,read_to_string};
use std::fmt::Write;
use std::vec::Vec;
use sha3::{Digest,Sha3_256};
use clap::{clap_app, crate_authors, crate_description, crate_version, AppSettings};
use rand::prelude::*;
use bitvec::prelude::*;

fn vectorize_key(key: &str) -> (Vec<&str>, Vec<&str>) {
    let mut key_0: Vec<&str> = Vec::with_capacity(64*8*32);
    let mut key_1: Vec<&str> = Vec::with_capacity(64*8*32);
    for (i, line) in key.lines().enumerate() {
        let mut tmp = line.trim();
        while tmp.len() > 0 {
            let (first, second) = tmp.split_at(64);
            if i < 32 {
                key_0.push(first);
            } else {
                key_1.push(first);
            }
            tmp = second;
        }
    }
    (key_0, key_1)
}

fn main() -> std::io::Result<()> {
    let matches = clap_app!(lamport_sign =>
        (setting: AppSettings::ArgRequiredElseHelp)
        (version: crate_version!())
        (author: crate_authors!())
        (about: crate_description!())
        (@subcommand keygen =>
            (about: "generate a pair of secret/public key")
            (@arg SK_FILE: -s +takes_value "Sets the name of the secret key file")
            (@arg PK_FILE: -p +takes_value "Sets the name of the public key file")
        )
        (@subcommand sign =>
            (about: "sign a file with a secret key")
            (@arg OUTPUT: -o +takes_value "Sets the name of the output file")
            (@arg SK_FILE: +required "Sets the secret key file to use")
            (@arg INPUT: +required "Sets the file to sign")
        )
        (@subcommand verify =>
            (about: "verify a file with a public key")
            (@arg PK_FILE: +required "Sets the public key file to use")
            (@arg SIGN: +required "Sets the signature file for the verification")
            (@arg INPUT: +required "Sets file to verify")
        )
    ).get_matches();
    match matches.subcommand() {
        ("keygen", Some(args)) => {
            let mut sk: Vec<u64> = Vec::with_capacity(256*2*4);
            let mut rng = thread_rng();
            sk.resize_with(256*2*4, | | rng.gen());
            let sk_name = args.value_of("SK_FILE").unwrap_or("ls_key.sk");
            let pk_name = args.value_of("PK_FILE").unwrap_or("ls_key.pk");
            let mut sk_file = File::create(sk_name)?;
            let mut pk_file = File::create(pk_name)?;
            let mut sk_buffer = String::new();
            sk.chunks_exact(8*4).map(|ck| { // 8 numeri da 256bit(32byte) in ogni riga (1u256 = 4u64)
                writeln!(&mut sk_buffer, "{}",
                         ck.iter().fold(String::with_capacity(64*8), // ogni u64 si può scrivere con 16 cifre (64 per u256)
                                        |mut acc, x| {
                                            write!(&mut acc, "{:016X}", x).unwrap(); acc })).unwrap();
            }).last();
            use std::io::Write;
            write!(&mut sk_file, "{}", sk_buffer)?;
            /*
            1 numero da 256 bit = 64 cifre esadecimali (8*4)
            Quello che dobbiamo fare è prendere ogni 32 cifre, ricompattarle in un numero,
            processare l'hash, e appenderlo nel file della public key
            */
            for line in sk_buffer.lines() {
                let mut tmp = line.trim();
                while tmp.len() > 0 {
                    let (first, second) = tmp.split_at(64);
                    // interpreto first e uso second per continuare il ciclo fino a che line non è vuoto
                    let mut hasher = Sha3_256::new();
                    hasher.input(first);
                    write!(&mut pk_file, "{:064X}", hasher.result())?;
                    tmp = second;
                }
                write!(&mut pk_file, "\n")?;
            }
            println!("Lamport Key Files {} and {} generated.", sk_name, pk_name);
        }
        ("sign", Some(args)) => {
            let sk_name = args.value_of("SK_FILE").unwrap();
            let input_name = args.value_of("INPUT").unwrap().to_owned();
            let out_name_default = format!("{}.sign", input_name);
            let out_name = args.value_of("OUTPUT").unwrap_or(out_name_default.as_str());
            let sk_str = read_to_string(sk_name)?;
            let (sk_0, sk_1) = vectorize_key(&sk_str);
            let input_str = read_to_string(&input_name)?;
            let mut hasher = Sha3_256::new();
            hasher.input(input_str);
            let input_hash_str = format!("{:064X}",hasher.result());
            let input_hash_bit = BitVec::<BigEndian,u8>::from_vec(hex::decode(input_hash_str).unwrap());
            let mut out_file = File::create(out_name)?;
            for (i, bit) in input_hash_bit.iter().enumerate() {
                use std::io::Write;
                if i % 8 == 0 && i != 0 { write!(&mut out_file, "\n")? }
                if bit {
                    write!(&mut out_file, "{}", sk_1[i])?;
                } else {
                    write!(&mut out_file, "{}", sk_0[i])?;
                }
            }
            println!("Signature with key {} of the file {} generated in {}", sk_name, input_name, out_name);
        }
        ("verify", Some(args)) => {
            let pk_name = args.value_of("PK_FILE").unwrap();
            let input_name = args.value_of("INPUT").unwrap();
            let sign_name = args.value_of("SIGN").unwrap();
            let pk_str = read_to_string(pk_name)?;
            let (pk_0, pk_1) = vectorize_key(&pk_str);

            let sign_str = read_to_string(sign_name)?;
            let mut expected_vec: Vec<String> = Vec::new(); // Constructed from the fragment of SK in signature
            for line in sign_str.lines().map(|ln| ln.trim() ) {
                let mut tmp = line;
                while tmp.len() > 0 {
                    let (first, second) = tmp.split_at(64);
                    let mut hasher = Sha3_256::new();
                    hasher.input(first);
                    expected_vec.push(format!("{:064X}",hasher.result()));
                    tmp = second;
                }
            }

            let input_str = read_to_string(input_name)?;
            let mut hasher = Sha3_256::new();
            hasher.input(input_str);
            let input_hash_str = format!("{:064X}",hasher.result());
            let input_hash_bit = BitVec::<BigEndian,u8>::from_vec(hex::decode(input_hash_str).unwrap());
            let mut verify_vec: Vec<&str> = Vec::new(); // Constructed from the public key and the
            for (i, bit) in input_hash_bit.iter().enumerate() {
                if bit {
                    verify_vec.push(pk_1[i]);
                } else {
                    verify_vec.push(pk_0[i]);
                }
            }
            let validated = expected_vec.iter().zip(verify_vec.iter()).all(|(e,&v)| e == v );
            if validated {
                println!("The file {} with the Signature {} is VALIDATED with the Public Key {}", input_name, sign_name, pk_name);
            } else {
                println!("The file {} with the Signature {} is NOT VALIDATED with the Public Key {}", input_name, sign_name, pk_name);
                exit(1);
            }
        }
        _ => {unreachable!()}
    }
    Ok(())
}
